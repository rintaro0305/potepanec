require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  given!(:taxonomy) { create(:taxonomy) }
  given!(:root_taxon) { taxonomy.root }
  given!(:child_taxon1) do
    create(:taxon, taxonomy: taxonomy,
                   parent_id: root_taxon.id,
                   name: "child_taxon1")
  end
  given!(:child_taxon2) do
    create(:taxon, taxonomy: taxonomy,
                   parent_id: root_taxon.id,
                   name: "child_taxon2")
  end
  given!(:child_taxon3) do
    create(:taxon, taxonomy: taxonomy,
                   parent_id: root_taxon.id,
                   name: "child_taxon3")
  end
  given!(:product1) { create(:product, taxons: [child_taxon1]) }
  given!(:product2) { create(:product, taxons: [child_taxon2]) }
  given!(:product3) { create(:product, taxons: [child_taxon3]) }

  scenario "page diplays all items that have correct taxon_id" do
    visit potepan_category_path(child_taxon1.id)

    expect(page).to have_content(child_taxon1.name)
    expect(page).to have_content(product1.name)
    expect(page).not_to have_content(product2.name)
    expect(page).not_to have_content(product3.name)
  end

  scenario "displayed product link brings to correct a product page" do
    visit potepan_category_path(child_taxon1.id)

    click_on product1.name
    expect(page).to have_current_path(potepan_product_path(product1.id))
    expect(page).to have_selector '.media-body h2', text: product1.name
    expect(page).to have_selector '.media-body h3', text: product1.display_price
  end
end
