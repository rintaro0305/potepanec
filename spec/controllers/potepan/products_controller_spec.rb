require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "get#show" do
    let(:product) { create(:product) }
    let(:product_property) { create(:product_property, product: product) }

    context "send valid params" do
      before { get :show, params: { id: product.id } }

      it "responds successfully" do
        expect(response).to be_success
      end

      it "returns status 200" do
        expect(response).to have_http_status "200"
      end

      it "returns correct product" do
        expect(assigns(:product)).to eq product
      end
    end

    context "send invalid params" do
      subject { get :show, params: { id: "hoge" } }

      it "raise error" do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
