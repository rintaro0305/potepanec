require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:root_taxon) { taxonomy.root }
  let!(:child_taxon) do
    create(:taxon, taxonomy: taxonomy,
                   parent_id: root_taxon.id,
                   name: "child_taxon")
  end
  let!(:child_taxon2) do
    create(:taxon, taxonomy: taxonomy,
                   parent_id: root_taxon.id,
                   name: "child_taxon2")
  end
  let!(:product) { create(:product, taxons: [child_taxon]) }
  let!(:product2) { create(:product, taxons: [child_taxon2]) }

  describe "get#show" do
    context "send valid params" do
      before do
        get :show, params: { id: child_taxon.id }
      end

      it "responds successfully" do
        expect(response).to be_success
      end

      it "returns status 200" do
        expect(response).to have_http_status "200"
      end

      it "returns correct taxon" do
        expect(assigns(:taxon)).to eq child_taxon
      end

      it "returns correct taxonomies" do
        expect(assigns(:taxonomies)).to match_array Spree::Taxonomy.all
      end

      it "retuns correnct product" do
        expect(assigns(:products)).to contain_exactly product
      end

      it "doesn't return incorrect product" do
        expect(assigns(:products)).not_to include product2
      end
    end

    context "send invalid params" do
      it "raise error" do
        expect { get :show, params: { id: "hoge" } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
